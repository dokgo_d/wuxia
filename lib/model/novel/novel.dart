import 'package:freezed_annotation/freezed_annotation.dart';

part 'novel.freezed.dart';
part 'novel.g.dart';

@freezed
class Novel with _$Novel {
  const factory Novel({
    required int id,
    String? name,
    bool? active,
    String? abbreviation,
    String? slug,
    String? language,
    String? languageAbbreviation,
    bool? visible,
    String? description,
    String? synopsis,
    String? coverUrl,
    String? translatorId,
    String? translatorUserName,
    String? authorName,
    bool? siteCreditsEnabled,
    String? teaserMessage,
    bool? isFree,
    bool? karmaActive,
    bool? novelHasSponsorPlans,
    bool? userHasEbook,
    bool? userHasNovelUnlocked,
    String? chapterGroups,
    List<String>? tags,
    List<String>? genres,
    String? sponsorPlans,
    String? latestAnnouncement,
    List<String>? ebooks,
  }) = _Novel;

  /// Generate Class from Map<String, dynamic>
  factory Novel.fromJson(Map<String, dynamic> json) => _$NovelFromJson(json);
}
