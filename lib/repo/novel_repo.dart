import 'package:wuxia/model/novel/novel_response.dart';
import 'package:wuxia/network/api_helper.dart';
import 'package:wuxia/network/exception/api_exception.dart';

class NovelRepo {
  final ApiHelper _helper = ApiHelper();

  Future<NovelResponse> fetchNovelList() async {
    try {
      final response = await _helper.get('novels');
      NovelResponse novelResponse = NovelResponse.fromJson(response);
      return novelResponse;
    } catch (e) {
      print("NovelRepo -> fetchNovelList -> error: $e");
      throw const FetchDataException();
    }
  }
}
