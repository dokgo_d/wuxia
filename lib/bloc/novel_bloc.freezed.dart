// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'novel_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$NovelEventTearOff {
  const _$NovelEventTearOff();

  FetchNovelEvent fetch() {
    return const FetchNovelEvent();
  }

  RefreshNovelEvent refresh() {
    return const RefreshNovelEvent();
  }
}

/// @nodoc
const $NovelEvent = _$NovelEventTearOff();

/// @nodoc
mixin _$NovelEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetch,
    required TResult Function() refresh,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? fetch,
    TResult Function()? refresh,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetch,
    TResult Function()? refresh,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchNovelEvent value) fetch,
    required TResult Function(RefreshNovelEvent value) refresh,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FetchNovelEvent value)? fetch,
    TResult Function(RefreshNovelEvent value)? refresh,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchNovelEvent value)? fetch,
    TResult Function(RefreshNovelEvent value)? refresh,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NovelEventCopyWith<$Res> {
  factory $NovelEventCopyWith(
          NovelEvent value, $Res Function(NovelEvent) then) =
      _$NovelEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$NovelEventCopyWithImpl<$Res> implements $NovelEventCopyWith<$Res> {
  _$NovelEventCopyWithImpl(this._value, this._then);

  final NovelEvent _value;
  // ignore: unused_field
  final $Res Function(NovelEvent) _then;
}

/// @nodoc
abstract class $FetchNovelEventCopyWith<$Res> {
  factory $FetchNovelEventCopyWith(
          FetchNovelEvent value, $Res Function(FetchNovelEvent) then) =
      _$FetchNovelEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$FetchNovelEventCopyWithImpl<$Res> extends _$NovelEventCopyWithImpl<$Res>
    implements $FetchNovelEventCopyWith<$Res> {
  _$FetchNovelEventCopyWithImpl(
      FetchNovelEvent _value, $Res Function(FetchNovelEvent) _then)
      : super(_value, (v) => _then(v as FetchNovelEvent));

  @override
  FetchNovelEvent get _value => super._value as FetchNovelEvent;
}

/// @nodoc

class _$FetchNovelEvent extends FetchNovelEvent {
  const _$FetchNovelEvent() : super._();

  @override
  String toString() {
    return 'NovelEvent.fetch()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is FetchNovelEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetch,
    required TResult Function() refresh,
  }) {
    return fetch();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? fetch,
    TResult Function()? refresh,
  }) {
    return fetch?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetch,
    TResult Function()? refresh,
    required TResult orElse(),
  }) {
    if (fetch != null) {
      return fetch();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchNovelEvent value) fetch,
    required TResult Function(RefreshNovelEvent value) refresh,
  }) {
    return fetch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FetchNovelEvent value)? fetch,
    TResult Function(RefreshNovelEvent value)? refresh,
  }) {
    return fetch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchNovelEvent value)? fetch,
    TResult Function(RefreshNovelEvent value)? refresh,
    required TResult orElse(),
  }) {
    if (fetch != null) {
      return fetch(this);
    }
    return orElse();
  }
}

abstract class FetchNovelEvent extends NovelEvent {
  const factory FetchNovelEvent() = _$FetchNovelEvent;
  const FetchNovelEvent._() : super._();
}

/// @nodoc
abstract class $RefreshNovelEventCopyWith<$Res> {
  factory $RefreshNovelEventCopyWith(
          RefreshNovelEvent value, $Res Function(RefreshNovelEvent) then) =
      _$RefreshNovelEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$RefreshNovelEventCopyWithImpl<$Res>
    extends _$NovelEventCopyWithImpl<$Res>
    implements $RefreshNovelEventCopyWith<$Res> {
  _$RefreshNovelEventCopyWithImpl(
      RefreshNovelEvent _value, $Res Function(RefreshNovelEvent) _then)
      : super(_value, (v) => _then(v as RefreshNovelEvent));

  @override
  RefreshNovelEvent get _value => super._value as RefreshNovelEvent;
}

/// @nodoc

class _$RefreshNovelEvent extends RefreshNovelEvent {
  const _$RefreshNovelEvent() : super._();

  @override
  String toString() {
    return 'NovelEvent.refresh()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is RefreshNovelEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetch,
    required TResult Function() refresh,
  }) {
    return refresh();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? fetch,
    TResult Function()? refresh,
  }) {
    return refresh?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetch,
    TResult Function()? refresh,
    required TResult orElse(),
  }) {
    if (refresh != null) {
      return refresh();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchNovelEvent value) fetch,
    required TResult Function(RefreshNovelEvent value) refresh,
  }) {
    return refresh(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FetchNovelEvent value)? fetch,
    TResult Function(RefreshNovelEvent value)? refresh,
  }) {
    return refresh?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchNovelEvent value)? fetch,
    TResult Function(RefreshNovelEvent value)? refresh,
    required TResult orElse(),
  }) {
    if (refresh != null) {
      return refresh(this);
    }
    return orElse();
  }
}

abstract class RefreshNovelEvent extends NovelEvent {
  const factory RefreshNovelEvent() = _$RefreshNovelEvent;
  const RefreshNovelEvent._() : super._();
}

/// @nodoc
class _$NovelStateTearOff {
  const _$NovelStateTearOff();

  InitialNovelState initial() {
    return const InitialNovelState();
  }

  FetchingNovelState fetching() {
    return const FetchingNovelState();
  }

  FetchedNovelState fetched({required List<Novel> novelList}) {
    return FetchedNovelState(
      novelList: novelList,
    );
  }

  ErrorNovelState error({required String message}) {
    return ErrorNovelState(
      message: message,
    );
  }
}

/// @nodoc
const $NovelState = _$NovelStateTearOff();

/// @nodoc
mixin _$NovelState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetching,
    required TResult Function(List<Novel> novelList) fetched,
    required TResult Function(String message) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNovelState value) initial,
    required TResult Function(FetchingNovelState value) fetching,
    required TResult Function(FetchedNovelState value) fetched,
    required TResult Function(ErrorNovelState value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NovelStateCopyWith<$Res> {
  factory $NovelStateCopyWith(
          NovelState value, $Res Function(NovelState) then) =
      _$NovelStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$NovelStateCopyWithImpl<$Res> implements $NovelStateCopyWith<$Res> {
  _$NovelStateCopyWithImpl(this._value, this._then);

  final NovelState _value;
  // ignore: unused_field
  final $Res Function(NovelState) _then;
}

/// @nodoc
abstract class $InitialNovelStateCopyWith<$Res> {
  factory $InitialNovelStateCopyWith(
          InitialNovelState value, $Res Function(InitialNovelState) then) =
      _$InitialNovelStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$InitialNovelStateCopyWithImpl<$Res>
    extends _$NovelStateCopyWithImpl<$Res>
    implements $InitialNovelStateCopyWith<$Res> {
  _$InitialNovelStateCopyWithImpl(
      InitialNovelState _value, $Res Function(InitialNovelState) _then)
      : super(_value, (v) => _then(v as InitialNovelState));

  @override
  InitialNovelState get _value => super._value as InitialNovelState;
}

/// @nodoc

class _$InitialNovelState extends InitialNovelState {
  const _$InitialNovelState() : super._();

  @override
  String toString() {
    return 'NovelState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is InitialNovelState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetching,
    required TResult Function(List<Novel> novelList) fetched,
    required TResult Function(String message) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNovelState value) initial,
    required TResult Function(FetchingNovelState value) fetching,
    required TResult Function(FetchedNovelState value) fetched,
    required TResult Function(ErrorNovelState value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialNovelState extends NovelState {
  const factory InitialNovelState() = _$InitialNovelState;
  const InitialNovelState._() : super._();
}

/// @nodoc
abstract class $FetchingNovelStateCopyWith<$Res> {
  factory $FetchingNovelStateCopyWith(
          FetchingNovelState value, $Res Function(FetchingNovelState) then) =
      _$FetchingNovelStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$FetchingNovelStateCopyWithImpl<$Res>
    extends _$NovelStateCopyWithImpl<$Res>
    implements $FetchingNovelStateCopyWith<$Res> {
  _$FetchingNovelStateCopyWithImpl(
      FetchingNovelState _value, $Res Function(FetchingNovelState) _then)
      : super(_value, (v) => _then(v as FetchingNovelState));

  @override
  FetchingNovelState get _value => super._value as FetchingNovelState;
}

/// @nodoc

class _$FetchingNovelState extends FetchingNovelState {
  const _$FetchingNovelState() : super._();

  @override
  String toString() {
    return 'NovelState.fetching()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is FetchingNovelState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetching,
    required TResult Function(List<Novel> novelList) fetched,
    required TResult Function(String message) error,
  }) {
    return fetching();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
  }) {
    return fetching?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (fetching != null) {
      return fetching();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNovelState value) initial,
    required TResult Function(FetchingNovelState value) fetching,
    required TResult Function(FetchedNovelState value) fetched,
    required TResult Function(ErrorNovelState value) error,
  }) {
    return fetching(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
  }) {
    return fetching?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
    required TResult orElse(),
  }) {
    if (fetching != null) {
      return fetching(this);
    }
    return orElse();
  }
}

abstract class FetchingNovelState extends NovelState {
  const factory FetchingNovelState() = _$FetchingNovelState;
  const FetchingNovelState._() : super._();
}

/// @nodoc
abstract class $FetchedNovelStateCopyWith<$Res> {
  factory $FetchedNovelStateCopyWith(
          FetchedNovelState value, $Res Function(FetchedNovelState) then) =
      _$FetchedNovelStateCopyWithImpl<$Res>;
  $Res call({List<Novel> novelList});
}

/// @nodoc
class _$FetchedNovelStateCopyWithImpl<$Res>
    extends _$NovelStateCopyWithImpl<$Res>
    implements $FetchedNovelStateCopyWith<$Res> {
  _$FetchedNovelStateCopyWithImpl(
      FetchedNovelState _value, $Res Function(FetchedNovelState) _then)
      : super(_value, (v) => _then(v as FetchedNovelState));

  @override
  FetchedNovelState get _value => super._value as FetchedNovelState;

  @override
  $Res call({
    Object? novelList = freezed,
  }) {
    return _then(FetchedNovelState(
      novelList: novelList == freezed
          ? _value.novelList
          : novelList // ignore: cast_nullable_to_non_nullable
              as List<Novel>,
    ));
  }
}

/// @nodoc

class _$FetchedNovelState extends FetchedNovelState {
  const _$FetchedNovelState({required this.novelList}) : super._();

  @override
  final List<Novel> novelList;

  @override
  String toString() {
    return 'NovelState.fetched(novelList: $novelList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchedNovelState &&
            const DeepCollectionEquality().equals(other.novelList, novelList));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(novelList));

  @JsonKey(ignore: true)
  @override
  $FetchedNovelStateCopyWith<FetchedNovelState> get copyWith =>
      _$FetchedNovelStateCopyWithImpl<FetchedNovelState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetching,
    required TResult Function(List<Novel> novelList) fetched,
    required TResult Function(String message) error,
  }) {
    return fetched(novelList);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
  }) {
    return fetched?.call(novelList);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (fetched != null) {
      return fetched(novelList);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNovelState value) initial,
    required TResult Function(FetchingNovelState value) fetching,
    required TResult Function(FetchedNovelState value) fetched,
    required TResult Function(ErrorNovelState value) error,
  }) {
    return fetched(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
  }) {
    return fetched?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
    required TResult orElse(),
  }) {
    if (fetched != null) {
      return fetched(this);
    }
    return orElse();
  }
}

abstract class FetchedNovelState extends NovelState {
  const factory FetchedNovelState({required List<Novel> novelList}) =
      _$FetchedNovelState;
  const FetchedNovelState._() : super._();

  List<Novel> get novelList;
  @JsonKey(ignore: true)
  $FetchedNovelStateCopyWith<FetchedNovelState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ErrorNovelStateCopyWith<$Res> {
  factory $ErrorNovelStateCopyWith(
          ErrorNovelState value, $Res Function(ErrorNovelState) then) =
      _$ErrorNovelStateCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class _$ErrorNovelStateCopyWithImpl<$Res> extends _$NovelStateCopyWithImpl<$Res>
    implements $ErrorNovelStateCopyWith<$Res> {
  _$ErrorNovelStateCopyWithImpl(
      ErrorNovelState _value, $Res Function(ErrorNovelState) _then)
      : super(_value, (v) => _then(v as ErrorNovelState));

  @override
  ErrorNovelState get _value => super._value as ErrorNovelState;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(ErrorNovelState(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ErrorNovelState extends ErrorNovelState {
  const _$ErrorNovelState({required this.message}) : super._();

  @override
  final String message;

  @override
  String toString() {
    return 'NovelState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ErrorNovelState &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  $ErrorNovelStateCopyWith<ErrorNovelState> get copyWith =>
      _$ErrorNovelStateCopyWithImpl<ErrorNovelState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetching,
    required TResult Function(List<Novel> novelList) fetched,
    required TResult Function(String message) error,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetching,
    TResult Function(List<Novel> novelList)? fetched,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNovelState value) initial,
    required TResult Function(FetchingNovelState value) fetching,
    required TResult Function(FetchedNovelState value) fetched,
    required TResult Function(ErrorNovelState value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNovelState value)? initial,
    TResult Function(FetchingNovelState value)? fetching,
    TResult Function(FetchedNovelState value)? fetched,
    TResult Function(ErrorNovelState value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ErrorNovelState extends NovelState {
  const factory ErrorNovelState({required String message}) = _$ErrorNovelState;
  const ErrorNovelState._() : super._();

  String get message;
  @JsonKey(ignore: true)
  $ErrorNovelStateCopyWith<ErrorNovelState> get copyWith =>
      throw _privateConstructorUsedError;
}
