import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'exception/api_exception.dart';

class ApiHelper {
  final String _baseUrl = "https://www.wuxiaworld.com/api/";

  static final ApiHelper _singleton = ApiHelper._internal();

  factory ApiHelper() {
    return _singleton;
  }

  ApiHelper._internal();

  Future<dynamic> get(String url) async {
    print('ApiHelper -> get -> url $url');
    var responseJson;
    try {
      final response = await http.get(Uri.parse(_baseUrl + url));
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No connection');
      throw const FetchDataException('No Internet connection');
    }
    print('ApiHelper -> get -> recieved');
    return responseJson;
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print("ApiHelper -> get -> responseJson");
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  //change for text builds
}
