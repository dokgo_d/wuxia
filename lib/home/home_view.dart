import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wuxia/bloc/novel_bloc.dart';
import 'package:wuxia/home/novel/novel_page.dart';
import 'package:wuxia/widget/error_view.dart';
import 'package:wuxia/widget/progress_view.dart';

@immutable
class HomeView extends StatefulWidget {
  const HomeView({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) =>
      super.debugFillProperties(
        properties
          ..add(
            StringProperty(
              'description',
              'HomeView StatefulWidget',
            ),
          ),
      );
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    super.initState();
    context.read<NovelBLoC>().add(const NovelEvent.fetch());
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) =>
      super.debugFillProperties(
        properties
          ..add(
            StringProperty(
              'description',
              '_HomeViewState State<HomeView>',
            ),
          ),
      );

  @override
  Widget build(BuildContext context) => Scaffold(
        body: BlocBuilder<NovelBLoC, NovelState>(builder: (context, state) {
          if (state is InitialNovelState) {
            return const ProgressView();
          } else if (state is FetchedNovelState) {
            return NovelPage(novelList: state.novelList);
          } else if (state is FetchingNovelState) {
            return const ProgressView();
          } else if (state is ErrorNovelState) {
            return const ErrorView();
          }

          return Container();
        }),
      );
}
