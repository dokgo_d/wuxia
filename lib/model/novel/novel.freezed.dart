// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'novel.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Novel _$NovelFromJson(Map<String, dynamic> json) {
  return _Novel.fromJson(json);
}

/// @nodoc
class _$NovelTearOff {
  const _$NovelTearOff();

  _Novel call(
      {@JsonKey(name: 'id', required: true, disallowNullValue: true)
          required int id,
      String? name,
      bool? active,
      String? abbreviation,
      String? slug,
      String? language,
      String? languageAbbreviation,
      bool? visible,
      String? description,
      String? synopsis,
      String? coverUrl,
      String? translatorId,
      String? translatorUserName,
      String? authorName,
      bool? siteCreditsEnabled,
      String? teaserMessage,
      bool? isFree,
      bool? karmaActive,
      bool? novelHasSponsorPlans,
      bool? userHasEbook,
      bool? userHasNovelUnlocked,
      String? chapterGroups,
      List<String>? tags,
      List<String>? genres,
      String? sponsorPlans,
      String? latestAnnouncement,
      List<String>? ebooks}) {
    return _Novel(
      id: id,
      name: name,
      active: active,
      abbreviation: abbreviation,
      slug: slug,
      language: language,
      languageAbbreviation: languageAbbreviation,
      visible: visible,
      description: description,
      synopsis: synopsis,
      coverUrl: coverUrl,
      translatorId: translatorId,
      translatorUserName: translatorUserName,
      authorName: authorName,
      siteCreditsEnabled: siteCreditsEnabled,
      teaserMessage: teaserMessage,
      isFree: isFree,
      karmaActive: karmaActive,
      novelHasSponsorPlans: novelHasSponsorPlans,
      userHasEbook: userHasEbook,
      userHasNovelUnlocked: userHasNovelUnlocked,
      chapterGroups: chapterGroups,
      tags: tags,
      genres: genres,
      sponsorPlans: sponsorPlans,
      latestAnnouncement: latestAnnouncement,
      ebooks: ebooks,
    );
  }

  Novel fromJson(Map<String, Object?> json) {
    return Novel.fromJson(json);
  }
}

/// @nodoc
const $Novel = _$NovelTearOff();

/// @nodoc
mixin _$Novel {
  @JsonKey(name: 'id', required: true, disallowNullValue: true)
  int get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  bool? get active => throw _privateConstructorUsedError;
  String? get abbreviation => throw _privateConstructorUsedError;
  String? get slug => throw _privateConstructorUsedError;
  String? get language => throw _privateConstructorUsedError;
  String? get languageAbbreviation => throw _privateConstructorUsedError;
  bool? get visible => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  String? get synopsis => throw _privateConstructorUsedError;
  String? get coverUrl => throw _privateConstructorUsedError;
  String? get translatorId => throw _privateConstructorUsedError;
  String? get translatorUserName => throw _privateConstructorUsedError;
  String? get authorName => throw _privateConstructorUsedError;
  bool? get siteCreditsEnabled => throw _privateConstructorUsedError;
  String? get teaserMessage => throw _privateConstructorUsedError;
  bool? get isFree => throw _privateConstructorUsedError;
  bool? get karmaActive => throw _privateConstructorUsedError;
  bool? get novelHasSponsorPlans => throw _privateConstructorUsedError;
  bool? get userHasEbook => throw _privateConstructorUsedError;
  bool? get userHasNovelUnlocked => throw _privateConstructorUsedError;
  String? get chapterGroups => throw _privateConstructorUsedError;
  List<String>? get tags => throw _privateConstructorUsedError;
  List<String>? get genres => throw _privateConstructorUsedError;
  String? get sponsorPlans => throw _privateConstructorUsedError;
  String? get latestAnnouncement => throw _privateConstructorUsedError;
  List<String>? get ebooks => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NovelCopyWith<Novel> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NovelCopyWith<$Res> {
  factory $NovelCopyWith(Novel value, $Res Function(Novel) then) =
      _$NovelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id', required: true, disallowNullValue: true) int id,
      String? name,
      bool? active,
      String? abbreviation,
      String? slug,
      String? language,
      String? languageAbbreviation,
      bool? visible,
      String? description,
      String? synopsis,
      String? coverUrl,
      String? translatorId,
      String? translatorUserName,
      String? authorName,
      bool? siteCreditsEnabled,
      String? teaserMessage,
      bool? isFree,
      bool? karmaActive,
      bool? novelHasSponsorPlans,
      bool? userHasEbook,
      bool? userHasNovelUnlocked,
      String? chapterGroups,
      List<String>? tags,
      List<String>? genres,
      String? sponsorPlans,
      String? latestAnnouncement,
      List<String>? ebooks});
}

/// @nodoc
class _$NovelCopyWithImpl<$Res> implements $NovelCopyWith<$Res> {
  _$NovelCopyWithImpl(this._value, this._then);

  final Novel _value;
  // ignore: unused_field
  final $Res Function(Novel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? active = freezed,
    Object? abbreviation = freezed,
    Object? slug = freezed,
    Object? language = freezed,
    Object? languageAbbreviation = freezed,
    Object? visible = freezed,
    Object? description = freezed,
    Object? synopsis = freezed,
    Object? coverUrl = freezed,
    Object? translatorId = freezed,
    Object? translatorUserName = freezed,
    Object? authorName = freezed,
    Object? siteCreditsEnabled = freezed,
    Object? teaserMessage = freezed,
    Object? isFree = freezed,
    Object? karmaActive = freezed,
    Object? novelHasSponsorPlans = freezed,
    Object? userHasEbook = freezed,
    Object? userHasNovelUnlocked = freezed,
    Object? chapterGroups = freezed,
    Object? tags = freezed,
    Object? genres = freezed,
    Object? sponsorPlans = freezed,
    Object? latestAnnouncement = freezed,
    Object? ebooks = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      active: active == freezed
          ? _value.active
          : active // ignore: cast_nullable_to_non_nullable
              as bool?,
      abbreviation: abbreviation == freezed
          ? _value.abbreviation
          : abbreviation // ignore: cast_nullable_to_non_nullable
              as String?,
      slug: slug == freezed
          ? _value.slug
          : slug // ignore: cast_nullable_to_non_nullable
              as String?,
      language: language == freezed
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String?,
      languageAbbreviation: languageAbbreviation == freezed
          ? _value.languageAbbreviation
          : languageAbbreviation // ignore: cast_nullable_to_non_nullable
              as String?,
      visible: visible == freezed
          ? _value.visible
          : visible // ignore: cast_nullable_to_non_nullable
              as bool?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      synopsis: synopsis == freezed
          ? _value.synopsis
          : synopsis // ignore: cast_nullable_to_non_nullable
              as String?,
      coverUrl: coverUrl == freezed
          ? _value.coverUrl
          : coverUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      translatorId: translatorId == freezed
          ? _value.translatorId
          : translatorId // ignore: cast_nullable_to_non_nullable
              as String?,
      translatorUserName: translatorUserName == freezed
          ? _value.translatorUserName
          : translatorUserName // ignore: cast_nullable_to_non_nullable
              as String?,
      authorName: authorName == freezed
          ? _value.authorName
          : authorName // ignore: cast_nullable_to_non_nullable
              as String?,
      siteCreditsEnabled: siteCreditsEnabled == freezed
          ? _value.siteCreditsEnabled
          : siteCreditsEnabled // ignore: cast_nullable_to_non_nullable
              as bool?,
      teaserMessage: teaserMessage == freezed
          ? _value.teaserMessage
          : teaserMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      isFree: isFree == freezed
          ? _value.isFree
          : isFree // ignore: cast_nullable_to_non_nullable
              as bool?,
      karmaActive: karmaActive == freezed
          ? _value.karmaActive
          : karmaActive // ignore: cast_nullable_to_non_nullable
              as bool?,
      novelHasSponsorPlans: novelHasSponsorPlans == freezed
          ? _value.novelHasSponsorPlans
          : novelHasSponsorPlans // ignore: cast_nullable_to_non_nullable
              as bool?,
      userHasEbook: userHasEbook == freezed
          ? _value.userHasEbook
          : userHasEbook // ignore: cast_nullable_to_non_nullable
              as bool?,
      userHasNovelUnlocked: userHasNovelUnlocked == freezed
          ? _value.userHasNovelUnlocked
          : userHasNovelUnlocked // ignore: cast_nullable_to_non_nullable
              as bool?,
      chapterGroups: chapterGroups == freezed
          ? _value.chapterGroups
          : chapterGroups // ignore: cast_nullable_to_non_nullable
              as String?,
      tags: tags == freezed
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      sponsorPlans: sponsorPlans == freezed
          ? _value.sponsorPlans
          : sponsorPlans // ignore: cast_nullable_to_non_nullable
              as String?,
      latestAnnouncement: latestAnnouncement == freezed
          ? _value.latestAnnouncement
          : latestAnnouncement // ignore: cast_nullable_to_non_nullable
              as String?,
      ebooks: ebooks == freezed
          ? _value.ebooks
          : ebooks // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
abstract class _$NovelCopyWith<$Res> implements $NovelCopyWith<$Res> {
  factory _$NovelCopyWith(_Novel value, $Res Function(_Novel) then) =
      __$NovelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id', required: true, disallowNullValue: true) int id,
      String? name,
      bool? active,
      String? abbreviation,
      String? slug,
      String? language,
      String? languageAbbreviation,
      bool? visible,
      String? description,
      String? synopsis,
      String? coverUrl,
      String? translatorId,
      String? translatorUserName,
      String? authorName,
      bool? siteCreditsEnabled,
      String? teaserMessage,
      bool? isFree,
      bool? karmaActive,
      bool? novelHasSponsorPlans,
      bool? userHasEbook,
      bool? userHasNovelUnlocked,
      String? chapterGroups,
      List<String>? tags,
      List<String>? genres,
      String? sponsorPlans,
      String? latestAnnouncement,
      List<String>? ebooks});
}

/// @nodoc
class __$NovelCopyWithImpl<$Res> extends _$NovelCopyWithImpl<$Res>
    implements _$NovelCopyWith<$Res> {
  __$NovelCopyWithImpl(_Novel _value, $Res Function(_Novel) _then)
      : super(_value, (v) => _then(v as _Novel));

  @override
  _Novel get _value => super._value as _Novel;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? active = freezed,
    Object? abbreviation = freezed,
    Object? slug = freezed,
    Object? language = freezed,
    Object? languageAbbreviation = freezed,
    Object? visible = freezed,
    Object? description = freezed,
    Object? synopsis = freezed,
    Object? coverUrl = freezed,
    Object? translatorId = freezed,
    Object? translatorUserName = freezed,
    Object? authorName = freezed,
    Object? siteCreditsEnabled = freezed,
    Object? teaserMessage = freezed,
    Object? isFree = freezed,
    Object? karmaActive = freezed,
    Object? novelHasSponsorPlans = freezed,
    Object? userHasEbook = freezed,
    Object? userHasNovelUnlocked = freezed,
    Object? chapterGroups = freezed,
    Object? tags = freezed,
    Object? genres = freezed,
    Object? sponsorPlans = freezed,
    Object? latestAnnouncement = freezed,
    Object? ebooks = freezed,
  }) {
    return _then(_Novel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      active: active == freezed
          ? _value.active
          : active // ignore: cast_nullable_to_non_nullable
              as bool?,
      abbreviation: abbreviation == freezed
          ? _value.abbreviation
          : abbreviation // ignore: cast_nullable_to_non_nullable
              as String?,
      slug: slug == freezed
          ? _value.slug
          : slug // ignore: cast_nullable_to_non_nullable
              as String?,
      language: language == freezed
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String?,
      languageAbbreviation: languageAbbreviation == freezed
          ? _value.languageAbbreviation
          : languageAbbreviation // ignore: cast_nullable_to_non_nullable
              as String?,
      visible: visible == freezed
          ? _value.visible
          : visible // ignore: cast_nullable_to_non_nullable
              as bool?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      synopsis: synopsis == freezed
          ? _value.synopsis
          : synopsis // ignore: cast_nullable_to_non_nullable
              as String?,
      coverUrl: coverUrl == freezed
          ? _value.coverUrl
          : coverUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      translatorId: translatorId == freezed
          ? _value.translatorId
          : translatorId // ignore: cast_nullable_to_non_nullable
              as String?,
      translatorUserName: translatorUserName == freezed
          ? _value.translatorUserName
          : translatorUserName // ignore: cast_nullable_to_non_nullable
              as String?,
      authorName: authorName == freezed
          ? _value.authorName
          : authorName // ignore: cast_nullable_to_non_nullable
              as String?,
      siteCreditsEnabled: siteCreditsEnabled == freezed
          ? _value.siteCreditsEnabled
          : siteCreditsEnabled // ignore: cast_nullable_to_non_nullable
              as bool?,
      teaserMessage: teaserMessage == freezed
          ? _value.teaserMessage
          : teaserMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      isFree: isFree == freezed
          ? _value.isFree
          : isFree // ignore: cast_nullable_to_non_nullable
              as bool?,
      karmaActive: karmaActive == freezed
          ? _value.karmaActive
          : karmaActive // ignore: cast_nullable_to_non_nullable
              as bool?,
      novelHasSponsorPlans: novelHasSponsorPlans == freezed
          ? _value.novelHasSponsorPlans
          : novelHasSponsorPlans // ignore: cast_nullable_to_non_nullable
              as bool?,
      userHasEbook: userHasEbook == freezed
          ? _value.userHasEbook
          : userHasEbook // ignore: cast_nullable_to_non_nullable
              as bool?,
      userHasNovelUnlocked: userHasNovelUnlocked == freezed
          ? _value.userHasNovelUnlocked
          : userHasNovelUnlocked // ignore: cast_nullable_to_non_nullable
              as bool?,
      chapterGroups: chapterGroups == freezed
          ? _value.chapterGroups
          : chapterGroups // ignore: cast_nullable_to_non_nullable
              as String?,
      tags: tags == freezed
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      sponsorPlans: sponsorPlans == freezed
          ? _value.sponsorPlans
          : sponsorPlans // ignore: cast_nullable_to_non_nullable
              as String?,
      latestAnnouncement: latestAnnouncement == freezed
          ? _value.latestAnnouncement
          : latestAnnouncement // ignore: cast_nullable_to_non_nullable
              as String?,
      ebooks: ebooks == freezed
          ? _value.ebooks
          : ebooks // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Novel implements _Novel {
  const _$_Novel(
      {@JsonKey(name: 'id', required: true, disallowNullValue: true)
          required this.id,
      this.name,
      this.active,
      this.abbreviation,
      this.slug,
      this.language,
      this.languageAbbreviation,
      this.visible,
      this.description,
      this.synopsis,
      this.coverUrl,
      this.translatorId,
      this.translatorUserName,
      this.authorName,
      this.siteCreditsEnabled,
      this.teaserMessage,
      this.isFree,
      this.karmaActive,
      this.novelHasSponsorPlans,
      this.userHasEbook,
      this.userHasNovelUnlocked,
      this.chapterGroups,
      this.tags,
      this.genres,
      this.sponsorPlans,
      this.latestAnnouncement,
      this.ebooks});

  factory _$_Novel.fromJson(Map<String, dynamic> json) =>
      _$$_NovelFromJson(json);

  @override
  @JsonKey(name: 'id', required: true, disallowNullValue: true)
  final int id;
  @override
  final String? name;
  @override
  final bool? active;
  @override
  final String? abbreviation;
  @override
  final String? slug;
  @override
  final String? language;
  @override
  final String? languageAbbreviation;
  @override
  final bool? visible;
  @override
  final String? description;
  @override
  final String? synopsis;
  @override
  final String? coverUrl;
  @override
  final String? translatorId;
  @override
  final String? translatorUserName;
  @override
  final String? authorName;
  @override
  final bool? siteCreditsEnabled;
  @override
  final String? teaserMessage;
  @override
  final bool? isFree;
  @override
  final bool? karmaActive;
  @override
  final bool? novelHasSponsorPlans;
  @override
  final bool? userHasEbook;
  @override
  final bool? userHasNovelUnlocked;
  @override
  final String? chapterGroups;
  @override
  final List<String>? tags;
  @override
  final List<String>? genres;
  @override
  final String? sponsorPlans;
  @override
  final String? latestAnnouncement;
  @override
  final List<String>? ebooks;

  @override
  String toString() {
    return 'Novel(id: $id, name: $name, active: $active, abbreviation: $abbreviation, slug: $slug, language: $language, languageAbbreviation: $languageAbbreviation, visible: $visible, description: $description, synopsis: $synopsis, coverUrl: $coverUrl, translatorId: $translatorId, translatorUserName: $translatorUserName, authorName: $authorName, siteCreditsEnabled: $siteCreditsEnabled, teaserMessage: $teaserMessage, isFree: $isFree, karmaActive: $karmaActive, novelHasSponsorPlans: $novelHasSponsorPlans, userHasEbook: $userHasEbook, userHasNovelUnlocked: $userHasNovelUnlocked, chapterGroups: $chapterGroups, tags: $tags, genres: $genres, sponsorPlans: $sponsorPlans, latestAnnouncement: $latestAnnouncement, ebooks: $ebooks)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Novel &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.active, active) &&
            const DeepCollectionEquality()
                .equals(other.abbreviation, abbreviation) &&
            const DeepCollectionEquality().equals(other.slug, slug) &&
            const DeepCollectionEquality().equals(other.language, language) &&
            const DeepCollectionEquality()
                .equals(other.languageAbbreviation, languageAbbreviation) &&
            const DeepCollectionEquality().equals(other.visible, visible) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.synopsis, synopsis) &&
            const DeepCollectionEquality().equals(other.coverUrl, coverUrl) &&
            const DeepCollectionEquality()
                .equals(other.translatorId, translatorId) &&
            const DeepCollectionEquality()
                .equals(other.translatorUserName, translatorUserName) &&
            const DeepCollectionEquality()
                .equals(other.authorName, authorName) &&
            const DeepCollectionEquality()
                .equals(other.siteCreditsEnabled, siteCreditsEnabled) &&
            const DeepCollectionEquality()
                .equals(other.teaserMessage, teaserMessage) &&
            const DeepCollectionEquality().equals(other.isFree, isFree) &&
            const DeepCollectionEquality()
                .equals(other.karmaActive, karmaActive) &&
            const DeepCollectionEquality()
                .equals(other.novelHasSponsorPlans, novelHasSponsorPlans) &&
            const DeepCollectionEquality()
                .equals(other.userHasEbook, userHasEbook) &&
            const DeepCollectionEquality()
                .equals(other.userHasNovelUnlocked, userHasNovelUnlocked) &&
            const DeepCollectionEquality()
                .equals(other.chapterGroups, chapterGroups) &&
            const DeepCollectionEquality().equals(other.tags, tags) &&
            const DeepCollectionEquality().equals(other.genres, genres) &&
            const DeepCollectionEquality()
                .equals(other.sponsorPlans, sponsorPlans) &&
            const DeepCollectionEquality()
                .equals(other.latestAnnouncement, latestAnnouncement) &&
            const DeepCollectionEquality().equals(other.ebooks, ebooks));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(id),
        const DeepCollectionEquality().hash(name),
        const DeepCollectionEquality().hash(active),
        const DeepCollectionEquality().hash(abbreviation),
        const DeepCollectionEquality().hash(slug),
        const DeepCollectionEquality().hash(language),
        const DeepCollectionEquality().hash(languageAbbreviation),
        const DeepCollectionEquality().hash(visible),
        const DeepCollectionEquality().hash(description),
        const DeepCollectionEquality().hash(synopsis),
        const DeepCollectionEquality().hash(coverUrl),
        const DeepCollectionEquality().hash(translatorId),
        const DeepCollectionEquality().hash(translatorUserName),
        const DeepCollectionEquality().hash(authorName),
        const DeepCollectionEquality().hash(siteCreditsEnabled),
        const DeepCollectionEquality().hash(teaserMessage),
        const DeepCollectionEquality().hash(isFree),
        const DeepCollectionEquality().hash(karmaActive),
        const DeepCollectionEquality().hash(novelHasSponsorPlans),
        const DeepCollectionEquality().hash(userHasEbook),
        const DeepCollectionEquality().hash(userHasNovelUnlocked),
        const DeepCollectionEquality().hash(chapterGroups),
        const DeepCollectionEquality().hash(tags),
        const DeepCollectionEquality().hash(genres),
        const DeepCollectionEquality().hash(sponsorPlans),
        const DeepCollectionEquality().hash(latestAnnouncement),
        const DeepCollectionEquality().hash(ebooks)
      ]);

  @JsonKey(ignore: true)
  @override
  _$NovelCopyWith<_Novel> get copyWith =>
      __$NovelCopyWithImpl<_Novel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_NovelToJson(this);
  }
}

abstract class _Novel implements Novel {
  const factory _Novel(
      {@JsonKey(name: 'id', required: true, disallowNullValue: true)
          required int id,
      String? name,
      bool? active,
      String? abbreviation,
      String? slug,
      String? language,
      String? languageAbbreviation,
      bool? visible,
      String? description,
      String? synopsis,
      String? coverUrl,
      String? translatorId,
      String? translatorUserName,
      String? authorName,
      bool? siteCreditsEnabled,
      String? teaserMessage,
      bool? isFree,
      bool? karmaActive,
      bool? novelHasSponsorPlans,
      bool? userHasEbook,
      bool? userHasNovelUnlocked,
      String? chapterGroups,
      List<String>? tags,
      List<String>? genres,
      String? sponsorPlans,
      String? latestAnnouncement,
      List<String>? ebooks}) = _$_Novel;

  factory _Novel.fromJson(Map<String, dynamic> json) = _$_Novel.fromJson;

  @override
  @JsonKey(name: 'id', required: true, disallowNullValue: true)
  int get id;
  @override
  String? get name;
  @override
  bool? get active;
  @override
  String? get abbreviation;
  @override
  String? get slug;
  @override
  String? get language;
  @override
  String? get languageAbbreviation;
  @override
  bool? get visible;
  @override
  String? get description;
  @override
  String? get synopsis;
  @override
  String? get coverUrl;
  @override
  String? get translatorId;
  @override
  String? get translatorUserName;
  @override
  String? get authorName;
  @override
  bool? get siteCreditsEnabled;
  @override
  String? get teaserMessage;
  @override
  bool? get isFree;
  @override
  bool? get karmaActive;
  @override
  bool? get novelHasSponsorPlans;
  @override
  bool? get userHasEbook;
  @override
  bool? get userHasNovelUnlocked;
  @override
  String? get chapterGroups;
  @override
  List<String>? get tags;
  @override
  List<String>? get genres;
  @override
  String? get sponsorPlans;
  @override
  String? get latestAnnouncement;
  @override
  List<String>? get ebooks;
  @override
  @JsonKey(ignore: true)
  _$NovelCopyWith<_Novel> get copyWith => throw _privateConstructorUsedError;
}
