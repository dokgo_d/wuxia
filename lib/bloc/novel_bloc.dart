import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:wuxia/model/novel/novel.dart';
import 'package:wuxia/model/novel/novel_response.dart';
import 'package:wuxia/network/exception/api_exception.dart';
import 'package:wuxia/repo/novel_repo.dart';

part 'novel_bloc.freezed.dart';

@freezed
abstract class NovelEvent with _$NovelEvent {
  const NovelEvent._();

  const factory NovelEvent.fetch() = FetchNovelEvent;

  const factory NovelEvent.refresh() = RefreshNovelEvent;
}

@freezed
abstract class NovelState with _$NovelState {
  const NovelState._();

  const factory NovelState.initial() = InitialNovelState;

  const factory NovelState.fetching() = FetchingNovelState;

  const factory NovelState.fetched({required List<Novel> novelList}) =
      FetchedNovelState;

  const factory NovelState.error({required String message}) = ErrorNovelState;
}

class NovelBLoC extends Bloc<NovelEvent, NovelState> {
  NovelBLoC() : super(const InitialNovelState()) {
    on<FetchNovelEvent>(_fetch);
    on<RefreshNovelEvent>(_refresh);
  }

  final NovelRepo _novelRepo = NovelRepo();

  Future _fetchNovelList(Emitter<NovelState> emit) async {
    emit(const NovelState.fetching());

    try {
      NovelResponse response = await _novelRepo.fetchNovelList();
      final List<Novel> novelList = response.items ?? [];
      emit(NovelState.fetched(novelList: novelList));
    } on FetchDataException {
      print("NovelBLoC -> _fetchNovelList -> error: FetchDataException");
      emit(const NovelState.error(message: 'FetchDataException'));
    } catch (ex) {
      print("NovelBLoC -> _fetchNovelList -> error: $ex");
      rethrow;
    }
  }

  Future _fetch(FetchNovelEvent event, Emitter<NovelState> emit) async {
    await _fetchNovelList(emit);
  }

  Future _refresh(RefreshNovelEvent event, Emitter<NovelState> emit) async {
    await _fetchNovelList(emit);
  }
}
