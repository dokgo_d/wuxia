import 'package:freezed_annotation/freezed_annotation.dart';

import 'novel.dart';

part 'novel_response.freezed.dart';

part 'novel_response.g.dart';

@freezed
class NovelResponse with _$NovelResponse {
  const factory NovelResponse(
      {int? total, List<Novel>? items, bool? result}) = _NovelResponse;

  /// Generate Class from Map<String, dynamic>
  factory NovelResponse.fromJson(Map<String, dynamic> json) =>
      _$NovelResponseFromJson(json);
}
