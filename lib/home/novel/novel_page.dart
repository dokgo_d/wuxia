import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:wuxia/bloc/novel_bloc.dart';
import 'package:wuxia/model/novel/novel.dart';

import 'novel_item_card.dart';

@immutable
class NovelPage extends StatefulWidget {
  final List<Novel> novelList;

  const NovelPage({
    Key? key,
    required this.novelList,
  }) : super(key: key);

  static _NovelPageState? of(BuildContext context) =>
      context.findAncestorStateOfType<_NovelPageState>();

  @override
  State<NovelPage> createState() => _NovelPageState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) =>
      super.debugFillProperties(
        properties
          ..add(
            StringProperty(
              'description',
              'NovelPage StatefulWidget',
            ),
          ),
      );
}

class _NovelPageState extends State<NovelPage> {
  late RefreshController _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) =>
      super.debugFillProperties(
        properties
          ..add(
            StringProperty(
              'description',
              '_NovelPageState State<NovelPage>',
            ),
          ),
      );

  @override
  Widget build(BuildContext context) => SmartRefresher(
        controller: _refreshController,
        onRefresh: _refresh,
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 3,
            crossAxisSpacing: 3,
            childAspectRatio: 0.65,
          ),
          itemBuilder: (context, index) => NovelItemCard(
            novel: widget.novelList[index],
          ),
          itemCount: widget.novelList.length,
        ),
      );

  void _refresh() {
    BlocProvider.of<NovelBLoC>(context).add(const NovelEvent.refresh());
  }
}
