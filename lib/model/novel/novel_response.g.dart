// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'novel_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NovelResponse _$$_NovelResponseFromJson(Map<String, dynamic> json) =>
    _$_NovelResponse(
      total: json['total'] as int?,
      items: (json['items'] as List<dynamic>?)
          ?.map((e) => Novel.fromJson(e as Map<String, dynamic>))
          .toList(),
      result: json['result'] as bool?,
    );

Map<String, dynamic> _$$_NovelResponseToJson(_$_NovelResponse instance) =>
    <String, dynamic>{
      'total': instance.total,
      'items': instance.items,
      'result': instance.result,
    };
