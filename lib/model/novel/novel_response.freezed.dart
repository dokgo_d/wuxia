// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'novel_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

NovelResponse _$NovelResponseFromJson(Map<String, dynamic> json) {
  return _NovelResponse.fromJson(json);
}

/// @nodoc
class _$NovelResponseTearOff {
  const _$NovelResponseTearOff();

  _NovelResponse call({int? total, List<Novel>? items, bool? result}) {
    return _NovelResponse(
      total: total,
      items: items,
      result: result,
    );
  }

  NovelResponse fromJson(Map<String, Object?> json) {
    return NovelResponse.fromJson(json);
  }
}

/// @nodoc
const $NovelResponse = _$NovelResponseTearOff();

/// @nodoc
mixin _$NovelResponse {
  int? get total => throw _privateConstructorUsedError;
  List<Novel>? get items => throw _privateConstructorUsedError;
  bool? get result => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NovelResponseCopyWith<NovelResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NovelResponseCopyWith<$Res> {
  factory $NovelResponseCopyWith(
          NovelResponse value, $Res Function(NovelResponse) then) =
      _$NovelResponseCopyWithImpl<$Res>;
  $Res call({int? total, List<Novel>? items, bool? result});
}

/// @nodoc
class _$NovelResponseCopyWithImpl<$Res>
    implements $NovelResponseCopyWith<$Res> {
  _$NovelResponseCopyWithImpl(this._value, this._then);

  final NovelResponse _value;
  // ignore: unused_field
  final $Res Function(NovelResponse) _then;

  @override
  $Res call({
    Object? total = freezed,
    Object? items = freezed,
    Object? result = freezed,
  }) {
    return _then(_value.copyWith(
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as int?,
      items: items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Novel>?,
      result: result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
abstract class _$NovelResponseCopyWith<$Res>
    implements $NovelResponseCopyWith<$Res> {
  factory _$NovelResponseCopyWith(
          _NovelResponse value, $Res Function(_NovelResponse) then) =
      __$NovelResponseCopyWithImpl<$Res>;
  @override
  $Res call({int? total, List<Novel>? items, bool? result});
}

/// @nodoc
class __$NovelResponseCopyWithImpl<$Res>
    extends _$NovelResponseCopyWithImpl<$Res>
    implements _$NovelResponseCopyWith<$Res> {
  __$NovelResponseCopyWithImpl(
      _NovelResponse _value, $Res Function(_NovelResponse) _then)
      : super(_value, (v) => _then(v as _NovelResponse));

  @override
  _NovelResponse get _value => super._value as _NovelResponse;

  @override
  $Res call({
    Object? total = freezed,
    Object? items = freezed,
    Object? result = freezed,
  }) {
    return _then(_NovelResponse(
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as int?,
      items: items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<Novel>?,
      result: result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_NovelResponse implements _NovelResponse {
  const _$_NovelResponse({this.total, this.items, this.result});

  factory _$_NovelResponse.fromJson(Map<String, dynamic> json) =>
      _$$_NovelResponseFromJson(json);

  @override
  final int? total;
  @override
  final List<Novel>? items;
  @override
  final bool? result;

  @override
  String toString() {
    return 'NovelResponse(total: $total, items: $items, result: $result)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _NovelResponse &&
            const DeepCollectionEquality().equals(other.total, total) &&
            const DeepCollectionEquality().equals(other.items, items) &&
            const DeepCollectionEquality().equals(other.result, result));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(total),
      const DeepCollectionEquality().hash(items),
      const DeepCollectionEquality().hash(result));

  @JsonKey(ignore: true)
  @override
  _$NovelResponseCopyWith<_NovelResponse> get copyWith =>
      __$NovelResponseCopyWithImpl<_NovelResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_NovelResponseToJson(this);
  }
}

abstract class _NovelResponse implements NovelResponse {
  const factory _NovelResponse({int? total, List<Novel>? items, bool? result}) =
      _$_NovelResponse;

  factory _NovelResponse.fromJson(Map<String, dynamic> json) =
      _$_NovelResponse.fromJson;

  @override
  int? get total;
  @override
  List<Novel>? get items;
  @override
  bool? get result;
  @override
  @JsonKey(ignore: true)
  _$NovelResponseCopyWith<_NovelResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
