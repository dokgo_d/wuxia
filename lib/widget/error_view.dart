import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';

@immutable
class ErrorView extends StatelessWidget {
  final String message;

  const ErrorView({
    Key? key,
    this.message = "Something went wrong",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: Text(
          message,
          style: const TextStyle(fontSize: 30),
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) =>
      super.debugFillProperties(
        properties
          ..add(
            StringProperty(
              'description',
              'ErrorView StatelessWidget',
            ),
          ),
      );
}
