import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';

@immutable
class ProgressView extends StatelessWidget {
  const ProgressView({
    Key? key,
  })  : super(key: key);

  @override
  Widget build(BuildContext context) =>
    const Center(
      child: CircularProgressIndicator(),
    );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) =>
      super.debugFillProperties(
        properties
          ..add(
            StringProperty(
              'description',
              'ProgressView StatelessWidget',
            ),
          ),
      );
}