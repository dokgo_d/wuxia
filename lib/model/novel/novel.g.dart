// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'novel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Novel _$$_NovelFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    requiredKeys: const ['id'],
    disallowNullValues: const ['id'],
  );
  return _$_Novel(
    id: json['id'] as int,
    name: json['name'] as String?,
    active: json['active'] as bool?,
    abbreviation: json['abbreviation'] as String?,
    slug: json['slug'] as String?,
    language: json['language'] as String?,
    languageAbbreviation: json['languageAbbreviation'] as String?,
    visible: json['visible'] as bool?,
    description: json['description'] as String?,
    synopsis: json['synopsis'] as String?,
    coverUrl: json['coverUrl'] as String?,
    translatorId: json['translatorId'] as String?,
    translatorUserName: json['translatorUserName'] as String?,
    authorName: json['authorName'] as String?,
    siteCreditsEnabled: json['siteCreditsEnabled'] as bool?,
    teaserMessage: json['teaserMessage'] as String?,
    isFree: json['isFree'] as bool?,
    karmaActive: json['karmaActive'] as bool?,
    novelHasSponsorPlans: json['novelHasSponsorPlans'] as bool?,
    userHasEbook: json['userHasEbook'] as bool?,
    userHasNovelUnlocked: json['userHasNovelUnlocked'] as bool?,
    chapterGroups: json['chapterGroups'] as String?,
    tags: (json['tags'] as List<dynamic>?)?.map((e) => e as String).toList(),
    genres:
        (json['genres'] as List<dynamic>?)?.map((e) => e as String).toList(),
    sponsorPlans: json['sponsorPlans'] as String?,
    latestAnnouncement: json['latestAnnouncement'] as String?,
    ebooks:
        (json['ebooks'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$$_NovelToJson(_$_Novel instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'active': instance.active,
      'abbreviation': instance.abbreviation,
      'slug': instance.slug,
      'language': instance.language,
      'languageAbbreviation': instance.languageAbbreviation,
      'visible': instance.visible,
      'description': instance.description,
      'synopsis': instance.synopsis,
      'coverUrl': instance.coverUrl,
      'translatorId': instance.translatorId,
      'translatorUserName': instance.translatorUserName,
      'authorName': instance.authorName,
      'siteCreditsEnabled': instance.siteCreditsEnabled,
      'teaserMessage': instance.teaserMessage,
      'isFree': instance.isFree,
      'karmaActive': instance.karmaActive,
      'novelHasSponsorPlans': instance.novelHasSponsorPlans,
      'userHasEbook': instance.userHasEbook,
      'userHasNovelUnlocked': instance.userHasNovelUnlocked,
      'chapterGroups': instance.chapterGroups,
      'tags': instance.tags,
      'genres': instance.genres,
      'sponsorPlans': instance.sponsorPlans,
      'latestAnnouncement': instance.latestAnnouncement,
      'ebooks': instance.ebooks,
    };
